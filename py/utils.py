import numpy


def new_array(n, dtype=numpy.object):
    return numpy.empty(n, dtype)
