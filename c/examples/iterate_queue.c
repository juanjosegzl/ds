#include <stdio.h>
#include <iterator.h>
#include <arrayqueue.h>

#define LOWER 0
#define UPPER 9

int main(int argc, char **argv) {
  int a = LOWER, b = UPPER;
  int i;

  if (argc > 1)
    a = atoi(argv[1]);
  if (argc > 2)
    b = atoi(argv[2]);

  arrayqueue_t queue;
  arrayqueue_init(&queue, sizeof(int));

  for(i=LOWER; i<=UPPER; i++)
    arrayqueue_enqueue(&queue, &i);

  iterator_t it;
  arrayqueue_iterator(&queue, &it, a, b);
  while(it.next(&it))
    printf("%d\n", *(int *)it.elem(&it));
  it.dispose(&it);
  arrayqueue_dispose(&queue);
  return 0;
}
