#include <stdio.h>
#include <arrayqueue.h>

int main() {
  int i;
  arrayqueue_t queue;
  arrayqueue_init(&queue, sizeof(int));

  for (i = 0; i < 10; i++)
    arrayqueue_enqueue(&queue, &i);

  while(queue.length > 0) {
    arrayqueue_dequeue(&queue, &i);
    printf("dequeued: %d\n", i);
  }
  arrayqueue_dispose(&queue);
  return 0;
}
