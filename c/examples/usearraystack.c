#include <stdio.h>
#include <arraystack.h>

int main() {
  int i;
  arraystack_t  stack;
  arraystack_init(&stack, sizeof(int));

  for (i=0; i<10; i++)
    arraystack_push(&stack, &i);

  while(stack.length > 0) {
    arraystack_pop(&stack, &i);
    printf("popped: %d\n", i);
  }
  arraystack_dispose(&stack);
  return(0);
}
