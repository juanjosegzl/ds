#include <stdio.h>
#include <iterator.h>
#include <arraystack.h>

#define LOWER 0
#define UPPER 9

int main(int argc, char **argv) {
  int a = LOWER, b = UPPER;
  int i;

  if (argc > 1)
    a = atoi(argv[1]);
  if (argc > 2)
    b = atoi(argv[2]);

  arraystack_t stack;
  arraystack_init(&stack, sizeof(int));

  for(i=LOWER; i<=UPPER; i++)
    arraystack_push(&stack, &i);

  iterator_t it;
  arraystack_iterator(&stack, &it, a, b);
  while(it.next(&it))
    printf("%d\n", *(int *)it.elem(&it));
  it.dispose(&it);
  arraystack_dispose(&stack);
  return 0;
}
