#include <stdio.h>
#include <sort.h>

#define LENGTH 9
int main() {
  int a[] = {3, 1, 4, 1, 5, 9, 2, 6, 5};
  for(int i=0; i < LENGTH; i++)
    printf("%c%d%c%s", i == 0 ? '{' : 0,a[i], i + 1 != LENGTH ? ',' : 0, i + 1 == LENGTH ? "}\n" : " ");

  quicksort(a, LENGTH);
  for(int i=0; i < LENGTH; i++)
    printf("%c%d%c%s", i == 0 ? '{' : 0,a[i], i + 1 != LENGTH ? ',' : 0, i + 1 == LENGTH ? "}\n" : " ");

  return 0;
}
