#include <stdio.h>
#include <stdlib.h>
#include <arraystack.h>

int is_balanced(char *s) {
  arraystack_t stack;
  arraystack_init(&stack, sizeof(char));
  int res = 0;
  char p;

  for(int i = 0; s[i] != 0; i++) {
    if ( s[i] == '(' || s[i] == '[' || s[i] == '{') {
      arraystack_push(&stack, &s[i]);
    } else {
      if (stack.length == 0)
	return 0; // Unbalanced
      arraystack_pop(&stack, &p);
      if (p == -1) return 0;
      if (s[i] == ')' && p != '(') return 0;
      if (s[i] == '}' && p != '{') return 0;
      if (s[i] == ']' && p != '[') return 0;
    }
  }
  res = stack.length == 0;
  arraystack_dispose(&stack);
  return res;
}

int main(){
  int t; 
  scanf("%d",&t);
  for(int a = 0; a < t; a++){
    char* s = (char *)malloc(10240 * sizeof(char));
    scanf("%s", s);
    printf("%s\n", is_balanced(s) ? "YES" : "NO");
  }
  return 0;
}
