#include <sort.h>

static int hoare_partition(int* array, int lo, int hi) {
  int pivot = array[lo];
  int i = lo - 1;
  int j = hi + 1;
  while (1) {
    do {i++;} while(array[i] < pivot);
    do {j--;} while(array[j] > pivot);
    if (i >= j)
      return j;
    int tmp = array[j];
    array[j] = array[i];
    array[i] = tmp;
  }
}

int lomuto_partition(int *array, int lo, int hi) {
  int pivot = array[hi];
  int i = lo - 1;
  for (int j = lo; j < hi - 1; j++) {
    if (array[j] < pivot) {
      i++;
      int tmp = array[i];
      array[i] = array[j];
      array[j] = tmp;
    }
  }
  int tmp = array[i+1];
  array[i+1] = array[hi];
  array[hi] = tmp;
  return i + 1;
}

void qsort(int* array, int lo, int hi) {
  if (lo < hi) {
    // TODO conditional compilation
    int p = hoare_partition(array, lo, hi);
    qsort(array, lo, p);
    qsort(array, p + 1, hi);
  }
}
