/*
 * Given two strings check if one is a permutation of another
 */

#include <assert.h>
#include <stdio.h>
#include <stdlib.h>

int compare(const void *a, const void *b) {
  return (*(char *)a - *(char *)b);
}

int sort_and_compare(char *first_string, char *second_string) {
  int len1 = 0, len2 = 0;

  /* O(n log n) */
  for(;0 != first_string[len1]; len1++);
  for(len2 = 0; 0 != second_string[len2]; len2++);

  if (len1 != len2)
    return 0;

  qsort(first_string, len1, sizeof(char), compare);
  qsort(second_string, len2, sizeof(char), compare);

  /* O(n) */
  for(; len1 > 0; len1--)
    if (first_string[len1-1] != second_string[len1-1])
      return 0;

  return 1;
}

int main(int argc, char **argv) {
  assert(argc == 3);
  char *first_string = argv[1];
  char *second_string = argv[2];

  printf("Is \"%s\" a permutation of \"%s\"?\n",
	 first_string, second_string);

  int res = sort_and_compare(first_string, second_string);

  printf("%s\n", res ? "YES" : "NO");
  return 0;
}
