/*
 * Determine if a string is a permutation of a palindrome
 */

#include <assert.h>
#include <stdio.h>
#include <stdlib.h>
#include <ctype.h>


int hashmap(char *str) {
  static int hashmap[26];
  for (char *c = str; *c; c++) {
    if (isalpha(*c)){
      if (islower(*c))
	hashmap[*c - 'a'] += 1;
      else if(isupper(*c))
	hashmap[*c - 'A'] += 1;
    }
  }

  int odd = 0;
  for (int i = 0; i < 26; i++) {
    if (hashmap[i] % 2 != 0)
      if(++odd > 1)
	return 0;
  }

  return 1;
}

int main(int argc, char **argv) {
  assert(argc == 2);
  char *str = argv[1];
  int res = hashmap(str);

  printf("Is \"%s\" a permutation of a palindrome?\n%s\n",
	 str, res ? "YES" : "NO");
  return 0;
}
