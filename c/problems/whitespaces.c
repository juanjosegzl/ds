#include <assert.h>
#include <stdio.h>
#include <string.h>


void replace_spaces(char *str) {
  int len = 0, num_spaces = 0;
  for (;str[len] != 0; len++) {
    if (str[len] == ' ')
      num_spaces++;
  }

  assert(num_spaces / 3 != 0);

  int trailing = num_spaces / 3 * 2;

  int j = len - 1;
  for (int i = len-1-trailing; i > 0; i--) {
    if (str[i] == ' ') {
      strncpy(str + j-2, "%20", 3);
      j -= 3;
    }
    else {
      str[j] = str[i];
      j--;
    }
  }
}

int main(int argc, char **argv) {
  assert(argc == 2);
  char *str = argv[1];

  printf("Original string: %s\n", str);
  replace_spaces(str);
  printf("Replaced spaces: %s\n", str);

  return 0;
}
