#include <assert.h>
#include <stdio.h>
#include <string.h>
#include <arrayqueue.h>
#include <stdlib.h>
#include <gmodule.h>

typedef struct {
  char letter;
  size_t count;
} compressed_t;

char *compress(char *string) {
  arrayqueue_t queue;
  arrayqueue_init(&queue, sizeof(compressed_t));

  compressed_t compressed_letter;
  compressed_letter.letter = string[0];
  compressed_letter.count = 1;

  size_t is_shorter = 0;

  for (int i = 1; i < strlen(string); i++) {
    if(compressed_letter.letter != string[i]) {
      arrayqueue_enqueue(&queue, &compressed_letter);

      compressed_letter.letter = string[i];
      compressed_letter.count = 1;
    }
    else {
      compressed_letter.count++;
      if (!is_shorter && compressed_letter.count > 2) {
	// This logic is wrong
	is_shorter = 1;
      }
    }
  }
  arrayqueue_enqueue(&queue, &compressed_letter);

  if (is_shorter) {
    GString* compressed_string = g_string_new(0);
    while (queue.length > 0) {
      arrayqueue_dequeue(&queue, &compressed_letter);
      g_string_append_printf(compressed_string,
			     "%c%d",
			     compressed_letter.letter,
			     compressed_letter.count);
    }
    return compressed_string->str;
  }
  return string;
}

int main(int argc, char **argv) {
  assert(argc == 2);
  char *string = argv[1];

  printf("Original string: %s\n", string);
  string = compress(string);
  printf("Compressed string: %s\n", string);
}
