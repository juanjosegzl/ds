#include <assert.h>
#include <string.h>
#include <stdlib.h>
#include <stdio.h>


int compare(char *first, char *second) {
  size_t first_len = strnlen(first, sizeof first);
  size_t second_len = strnlen(second, sizeof second);

  if (abs(first_len - second_len) > 1)
    return 0;

  size_t diff = 0;

  if (first_len == second_len) {
    for(size_t i = 0; i < first_len; i++) {
      if (first[i] != second[i]) {
	diff += 1;
	if (diff > 1)
	  return 0;
      }
    }
  }
  else {
    char *largest = first_len > second_len ? first : second;
    char *shortest = first_len < second_len ? first : second;

    size_t largest_len = first_len > second_len ? first_len : second_len;
    size_t j = 0;
    for (size_t i = 0; i < largest_len; i++) {
      if (largest[i] != shortest[i]) {
	diff += 1;
	if (diff > 1)
	  return 0;
      }
      else {
	j++;
      }
    }
  }
  return 1;
}

int main(int argc, char **argv) {
  assert(argc == 3);

  char *first = argv[1];
  char *second = argv[2];

  int res = compare(first, second);
  printf("Are \"%s\" and \"%s\" one edit away?\n%s\n",
	 first, second, res ? "YES" : "NO");
}
