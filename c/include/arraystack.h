#ifndef ARRAYSTACK_H
#define ARRAYSTACK_H

#include <stdlib.h>
#include <iterator.h>

#define arraystack_push(s, elem)\
  arraystack_add((s), (s)->length, (elem))

#define arraystack_pop(s, elem_out)\
  arraystack_remove((s), (s)->length - 1, (elem_out))

typedef struct {
  size_t alloc_length;
  size_t length;
  size_t elem_size;
  void* array;
} arraystack_t;

extern void arraystack_add(arraystack_t* s,
			   size_t pos,
			   void* elem);

extern void arraystack_clear(arraystack_t* s);

extern void arraystack_copy(arraystack_t* dest,
			    size_t dest_pos,
			    arraystack_t* src,
			    size_t src_pos,
			    size_t num_elems);

extern void arraystack_dispose(arraystack_t* s);

extern void arraystack_get(arraystack_t* s,
			   size_t pos,
			   void* elem_out);

extern void arraystack_init(arraystack_t* s,
			    size_t elem_size);

extern void arraystack_iterator(arraystack_t* s,
				iterator_t* it,
				size_t start,
				size_t end);

extern void arraystack_remove(arraystack_t* s,
			      size_t pos,
			      void* elem_out);

extern void arraystack_reserve(arraystack_t* s,
				size_t n);

extern void arraystack_reverse(arraystack_t* s,
			       size_t pos,
			       size_t num_elems);

extern void arraystack_set(arraystack_t* s,
			   size_t pos,
			   void* elem,
			   void* old_elem);

extern void arraystack_truncate(arraystack_t* s,
				size_t pos,
				size_t num_elems);

#endif
