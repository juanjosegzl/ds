#ifndef ITERATOR_H
#define ITERATOR_H

typedef struct iterator_t {
  int (*next)(struct iterator_t*);
  void* (*elem)(struct iterator_t*) ;
  void* (*dispose)(struct iterator_t*);
  void* istruct;
} iterator_t;

#endif
