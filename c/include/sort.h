#ifndef SORT_H
#define SORT_H

#define quicksort(a, l) \
  qsort((a), 0, (l) - 1)

extern void qsort(int* array, int lo, int hi);

#endif
