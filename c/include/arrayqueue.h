#ifndef ARRAYQUEUE_H
#define ARRAYQUEUE_H

#include <stdlib.h>
#include <iterator.h>

typedef struct {
  size_t alloc_length;
  size_t length;
  size_t pos;
  size_t elem_size;
  void* array;
} arrayqueue_t;

extern void arrayqueue_clear(arrayqueue_t* q);

extern void arrayqueue_dequeue(arrayqueue_t* q, void* elem_out);

extern void arrayqueue_dispose(arrayqueue_t* q);

extern void arrayqueue_enqueue(arrayqueue_t* q, void* elem);

extern void arrayqueue_init(arrayqueue_t* q, size_t elem_size);

extern void arrayqueue_iterator(arrayqueue_t* q,
				iterator_t* it,
				size_t start,
				size_t end);

extern void arrayqueue_peek(arrayqueue_t* q, void* elem_out);

#endif
